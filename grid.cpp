#include<iostream>
#include<unistd.h> //library for clear display.

using namespace std;

void display(); //output connect 4 table.
void win_check(); //check all case to win(end loop).
void input(); //put Player's symbol to table.

	int grid[6][7]={};
	int x=0,time=1; //x for end loop when some player win , time for change number in array and about output player1=x,player2=o.
	int draw; //position on grid that player want to put own symbol.

int main(){

	while(x!=1){
		system("cls");
		
		display();
		win_check();
    	if(x==1){      //if player win skip input.
    		continue;
		}
		input();
	}

	if(time==1){
		cout<<"\nPlayer 2 win";
	}else{
		cout<<"\nPlayer 1 win";
	}
	
	return 0;
}

void display(){
	cout<<"   1    2    3    4    5    6    7\n\n";
		for(int i=0;i<6;i++){
			for(int j=0;j<7;j++){
				cout<<"||";
					if(grid[i][j]==1){
					cout<<" x ";
				}else if(grid[i][j]==2){
					cout<<" o ";
				}else{
				cout<<"   ";
				}
			}
		cout<<"||"<<endl;
		}
}

void win_check(){
	for(int j = 0; j<6; j++){
        for(int i = 0; i<7; i++){
        	if(grid[j][i]==1 && grid[j+1][i+1]==1 && grid[j+2][i+2]==1 && grid[j+3][i+3]==1){
       	    	 x=1;
     	    }
        	if(grid[j][i]==1 && grid[j+1][i-1]==1 && grid[j+2][i-2]==1 && grid[j+3][i-3]==1){
            	x=1;
            }
        	if(grid[j][i]==2 && grid[j+1][i-1]==2 && grid[j+2][i-2]==2 && grid[j+3][i-3]==2){
           		x=1;
        	}else if(grid[j][i]==2 && grid[j-1][i-1]==2 && grid[j-2][i-2]==2 && grid[j-3][i-3]==2){
            	x=1;
        	}else if(grid[j][i]==1 && grid[j][i-1]==1 && grid[j][i-2]==1 && grid[j][i-3]==1){
            	x=1;
			}else if(grid[j][i]==1 && grid[j-1][i]==1 && grid[j-2][i]==1 && grid[j-3][i]==1){
            	x=1;
        	}else if(grid[j][i]==2 && grid[j][i-1]==2 && grid[j][i-2]==2 && grid[j][i-3]==2){
            	x=1;
        	}else if(grid[j][i]==2 && grid[j-1][i]==2 && grid[j-2][i]==2 && grid[j-3][i]==2){
            	x=1;
        	}
        }
    }
}

void input(){
	if(time==2){
		cout<<"\nPlayer2 key number(1-7): ";
	}else{
		cout<<"\nPlayer1 key number(1-7): ";
	}
	cin>>draw;
	draw=draw-1;//reduce number which their input to be relate with position in array.
	
	if(grid[5][draw]==0){
		grid[5][draw]=time;
	}else if(grid[4][draw]==0){
		grid[4][draw]=time;
	}else if(grid[3][draw]==0){
		grid[3][draw]=time;
	}else if(grid[2][draw]==0){
		grid[2][draw]=time;
	}else if(grid[1][draw]==0){
		grid[1][draw]=time;
	}else if(grid[0][draw]==0){
		grid[0][draw]=time;
	}
	
	if(time==1){
		time++;
	}else{
		time--;
	}
}




